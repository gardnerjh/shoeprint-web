var geocoder = new google.maps.Geocoder();
var directionsService = new google.maps.DirectionsService();
var elevationService = new google.maps.ElevationService();
var elevationChart;
var map;
var marker;
var polyline;
var routes = [];
var aggregate_overview_path = [];
var aggregate_distance = 0;
var address_entered = false;

function initializeMap() {
	centerMap(new google.maps.LatLng(40.339909, -97.927915), 4);
	elevationChart = new google.visualization.LineChart(document.getElementById('elevation'));
	$('#addressModal').modal('show');

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			$('#addressModal').modal('hide');
			centerMap(new google.maps.LatLng(position.coords.latitude, position.coords.longitude), 15);
			address_entered = true;
		});
	}
}

function centerMap(latLngObject, zoomLevel) {
	var mapCanvas = document.getElementById('map_canvas');
	var mapOptions = {
		center: latLngObject,
		zoom: zoomLevel,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI: true,
		zoomControl: true,
		scaleControl: true
	};
	map = new google.maps.Map(mapCanvas, mapOptions);
	
	google.maps.event.addListener(map, 'click', function(event) {
		if (address_entered) {
			routePointAdded(event.latLng, 'map');
		}
	});
}

function routePointAdded(latLngObject, objClicked) {
	allRoutesLoaded = false;
	var currentRouteIndex = routes.length - 1;
	if (currentRouteIndex < 0) {
		// This is the starting point
		routes.push(
			{
				startingPoint:latLngObject
			}
		);
		setMarker(latLngObject);
	} else {
		if (routes[currentRouteIndex].endingPoint != null) {
			// There is already a route on the map--this is a waypoint
			
			if (routes[currentRouteIndex].waypoints == null) {
				// There aren't any waypoints yet; initialize waypoints array
				routes[currentRouteIndex].waypoints = [];
			}
			
			if (routes[currentRouteIndex].waypoints.length == 8) {
				// We have already reached the waypoints limit; need to end this route and start a new one
				routes.push(
					{
						startingPoint: routes[currentRouteIndex].endingPoint
					}
				);
				currentRouteIndex++;
			} else {
				// Push current endingPoint to the waypoints array
				routes[currentRouteIndex].waypoints.push(
					{
						location: routes[currentRouteIndex].endingPoint, 
						stopover: false
					}
				);
			}
		}
		
		var endingPoint;
		if (objClicked == 'marker') {
			// Use route starting point as endingPoint
			endingPoint = routes[0].startingPoint;
		} else {
			endingPoint = latLngObject;
		}
		
		routes[currentRouteIndex].endingPoint = endingPoint;
		drawDirections();
	}
}

function setMarker(latLngObject) {
	removeMarker();
	
	marker = new google.maps.Marker({
		map: map,
		position: latLngObject
	});
	
	google.maps.event.addListener(marker, 'click', function(event) {
		routePointAdded(event, 'marker');
	});
}

function drawDirections() {
	clearMap(false);
	
	if (routes.length > 0) {
		setMarker(routes[0].startingPoint);
		
		if (routes[0].endingPoint != null) {
			// We have a route to draw
			var isFirstRoute = true; // isFirstRoute variable is used to filter out unnecessary route steps from Google
			$.each(routes, function(i, val) {
				var currentRoute = val;
				var request = {
					origin: val.startingPoint,
					destination: val.endingPoint,
					waypoints: val.waypoints,
					travelMode: google.maps.TravelMode.WALKING
				};
				directionsService.route(request, function(result, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						currentRoute.overview_path = result.routes[0].overview_path;
						aggregate_overview_path = aggregate_overview_path.concat(currentRoute.overview_path);
						currentRoute.distance = getRouteDistanceInMeters(result);
						aggregate_distance += currentRoute.distance;
						currentRoute.steps = getRouteSteps(result, isFirstRoute);
					} else {
						alert('Could not get directions.<br />' + status);
					}
					isFirstRoute = false;
					
					if (i == (routes.length - 1)) {
						// This is the last route--we can update map now
						updateMap();
					}
				});
			});
		} else {
			hideTabPanel();
		}
	} else {
		clearMap(true);
	}
}

function updateMap() {
	drawPolyline();
	updateDistanceLabel();
	updateRouteSteps();
	updateElevation();
	$('#tabPanel').show();
}

function getRouteSteps(directionsResult, isFirstRoute) {
	var steps = [];
	$.each(directionsResult.routes, function() {
		$.each(this.legs, function() {
			$.each(this.steps, function(i, val) {
				if (isFirstRoute || i > 0) {
					// Skips the first step for route>1--first step "Head north on road..." is unnecessary
					steps.push(
						{
							instructions: this.instructions.replace(/<.*?>/g,'').replace(/Destination will be on the (left|right)/,''), 
							distance: this.distance.value
						}
					);
				}
			});
		});
	});
	return steps;
}

function updateElevation() {
	var samples = 10;
	if (aggregate_distance > 500) {
		samples = aggregate_distance / 50;
	}
	
	var pathRequest = {
		path: aggregate_overview_path,
		samples: samples
	}
	
	elevationService.getElevationAlongPath(pathRequest, function(results, status) {
		if (status == google.maps.ElevationStatus.OK) {
			var dataTable = new google.visualization.DataTable();
			dataTable.addColumn('string', 'Sample');
			dataTable.addColumn('number', 'Elevation');
			$.each(results, function() {
				dataTable.addRow(['', this.elevation]);
			});
			
			elevationChart.draw(dataTable, {
				curveType: 'function',
				legend: { position: 'none' }
			});
		}
	});
}

function updateRouteSteps() {
	$('#stepsTable').empty();
	var unitType = $('#unitType').text();
	var stepNumber = 0;
	
	$.each(routes, function() {
		$.each(this.steps, function() {
			stepNumber += 1;
			var distanceInUnits = convertMetersToUnits(this.distance, unitType);
			$('#stepsTable').append('<tr><td>' + stepNumber + '</td><td>' + this.instructions + '</td><td>' + distanceInUnits + ' ' + unitType + '</td></tr>');
		});
	});
}

function updateDistanceLabel() {
	var unitType = $('#unitType').text();
	var totalUnits = convertMetersToUnits(aggregate_distance, unitType);
	$('#distance').text(totalUnits);
}

function drawPolyline() {
	var line = new google.maps.Polyline({
		path: aggregate_overview_path,
		geodesic: true,
		strokeColor: '#0000FF',
		strokeOpacity: 0.5,
		strokeWeight: 5
	});
	
	line.setMap(map);

	polyline = line;
}

function changeMapAddress(address) {
	geocoder.geocode( { 'address': address }, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			centerMap(results[0].geometry.location, 15);
			address_entered = true;
		} else {
			alert("Geocode was not successful for the following reason: " + status);
		}
	});
}

function getRouteDistanceInMeters(directionsResult) {
	var routeDistance = 0;
	$.each(directionsResult.routes[0].legs, function() {
		routeDistance += this.distance.value;
	});
	return routeDistance;
}

function undoLastClick() {
	var routesLastIndex = routes.length - 1;
	if (routesLastIndex < 0 || routes[routesLastIndex].endingPoint == null) {
		// Marker (or nothing) is only thing on map, so clear everything out
		clearMap(true);
	} else {
		// There are route(s) on the map
		
		if (routes[routesLastIndex].waypoints != null && routes[routesLastIndex].waypoints.length > 0) {
			// Last draw was adding to an existing route, so set the last waypoint as the new endingPoint for this route and redraw
			var waypointsLastIndex = routes[routesLastIndex].waypoints.length - 1;
			routes[routesLastIndex].endingPoint = routes[routesLastIndex].waypoints[waypointsLastIndex].location;
			routes[routesLastIndex].waypoints.pop();
		} else {
			// Last draw was a new route thus should be removed from the map
			polyline.setMap(null);
			if (routesLastIndex > 0) {
				// This was the first leg of a non-starting route, so remove it
				routes.pop();
			} else {
				// This was the first leg of the starting route, so keep startingPoint
				clearOutAllButStartingPoint(routes[0]);
				hideTabPanel();
			}
		}
		
		// Re-draw map
		drawDirections();
	}
}

function clearOutAllButStartingPoint(route) {
	delete route.distance;
	delete route.endingPoint;
	delete route.steps;
}

function clearMap(clearRoutes) {
	removeMarker();
	removePolyline();
	aggregate_overview_path = [];
	aggregate_distance = 0;
	
	if (clearRoutes) {
		routes = [];
		hideTabPanel();
		updateDistanceLabel(0);
	}
}

function removePolyline() {
	if (polyline != null) {
		polyline.setMap(null);
	}
}

function removeMarker() {
	if (marker != null) {
		marker.setMap(null);
	}
}

function hideTabPanel() {
	$('#tabPanel').hide();
	$('#stepsTable').empty();
}

function convertMetersToUnits(meters, unitType) {
	var units;
	
	if (meters == 0) {
		return 0;
	}
	
	switch (unitType) {
		case 'ft':
			units = meters * 3.28084;
			break;
		case 'yd':
			units = meters * 1.09361;
			break;
		case 'm':
			units = meters;
			break;
		case 'km':
			units = meters * 0.001;
			break;
		case 'mi':
			units = meters * 0.000621371;
			break;
	}
	
	return Math.round(units * 100) / 100;
}

function convertUnitsToMeters(units, unitType) {
	var meters;
	
	if (units == 0) {
		return 0;
	}

	switch (unitType) {
		case 'ft':
			meters = units * 0.3048;
			break;
		case 'yd':
			meters = units * 0.9144;
			break;
		case 'm':
			meters = units;
			break;
		case 'km':
			meters = units * 1000;
			break;
		case 'mi':
			meters = units * 1609.34;
			break;
	}
	
	return Math.round(meters * 100) / 100;
}