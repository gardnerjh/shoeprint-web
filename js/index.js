$(document).ready(function() {
	initializeMap();

	$('#addressChangeBtn').click(function(){
		var address = $('#address').val();
		changeMapAddress(address);
	});

	$('#undo').click(undoLastClick);
	
	$('#clear').click(function() {
		$('#distance').text(0);
		clearMap(true);
	});
	
	$('#unitTypeMenu li a').click(function() {
		var previousText = $('#unitType').html();
		var selectedText = $(this).text();
		if (previousText != selectedText) {
			$('#unitType').html(selectedText);
			updateDistanceLabel();
		}
	});
});