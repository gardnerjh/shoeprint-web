<?php

date_default_timezone_set('America/New_York');

class Deploy {

  /**
  * A callback function to call after the deploy has finished.
  * 
  * @var callback
  */
  public $post_deploy;
  
  /**
  * The name of the file that will be used for logging deployments. Set to 
  * FALSE to disable logging.
  * 
  * @var string
  */
  private $_log = '/var/www/logs/deploy-shoeprint.log';

  /**
  * The timestamp format used for logging.
  * 
  * @link    http://www.php.net/manual/en/function.date.php
  * @var     string
  */
  private $_date_format = 'Y-m-d H:i:sP';

  /**
  * The name of the branch to pull from.
  * 
  * @var string
  */
  private $_branch = 'master';

  /**
  * The name of the remote to pull from.
  * 
  * @var string
  */
  private $_remote = 'origin';

  /**
  * The directory where your website and git repository are located, can be 
  * a relative or absolute path.
  * 
  * @var string
  */
  private $_directory;

  /**
  * Whether the deployment was successful or not. Considered successful
  * until failure.
  *
  * @var boolean
  */
  private $_success = TRUE;

  /**
  * Sets up defaults.
  * 
  * @param  string  $directory  Directory where your website is located
  * @param  array   $data       Information about the deployment
  */
  public function __construct($directory, $options = array())
  {
    // Determine the directory path
    $this->_directory = realpath($directory).DIRECTORY_SEPARATOR;

    $available_options = array('log', 'date_format', 'branch', 'remote');

    foreach ($options as $option => $value)
    {
        if (in_array($option, $available_options))
        {
            $this->{'_'.$option} = $value;
        }
    }

    $this->log('Attempting deployment...');
  }

  /**
  * Writes a message to the log file.
  * 
  * @param  string  $message  The message to write
  * @param  string  $type     The type of log message (e.g. INFO, DEBUG, ERROR, etc.)
  */
  public function log($message, $type = 'INFO')
  {
    if ($this->_log)
    {
        // Set the name of the log file
        $filename = $this->_log;

        if ( ! file_exists($filename))
        {
            // Create the log file
            file_put_contents($filename, '');

            // Allow anyone to write to log files
            chmod($filename, 0666);
        }

        // Write the message into the log file
        // Format: time --- type: message
        file_put_contents($filename, date($this->_date_format).' --- '.$type.': '.$message.PHP_EOL, FILE_APPEND);
    }
  }

  /**
  * Executes the necessary commands to deploy the website.
  */
  public function execute()
  {
    try {
        // Make sure we're in the right directory
        exec('cd '.$this->_directory.' 2>&1', $output, $return);
        if (!$return) {
          $this->log('Changing working directory... '.implode(' ', $output));
        } else {
          throw new Exception('Unable to change working directory... '.implode(' ', $output));
        }

        // Discard any changes to tracked files since our last deploy
        exec('git reset --hard HEAD 2>&1', $output, $return);
        if (!$return) {
          $this->log('Resetting repository... '.implode(' ', $output));
        } else {
          throw new Exception('Unable to reset repository... '.implode(' ', $output));
        }

        // Update the local repository
        exec('git pull '.$this->_remote.' '.$this->_branch.' 2>&1', $output, $return);
        if (!$return) {
          $this->log('Pulling in changes... '.implode(' ', $output));
        } else {
          throw new Exception('Unable to pull in changes... '.implode(' ', $output));
        }
        
        // Secure the .git directory
        exec('chmod -R og-rx .git 2>&1', $output);
        $this->log('Securing .git directory... '.implode(' ', $output));

        $this->log('Deployment successful.');
    } catch (Exception $e) {
        $this->log($e, 'ERROR');
        $this->_success = FALSE;
    } finally {
      if (is_callable($this->post_deploy)) {
          call_user_func($this->post_deploy, $this->_data);
      }
    }
  }
}

try {
  $deploy = new Deploy('/var/www/html/shoeprint');

  try {
    // Only deploy on POST requests
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $json = json_decode($_POST["payload"]);
      // Only deploy when commit was made to target branch
      if (count($json->commits) > 0 && $json->commits[0]->branch == $deploy->_branch) {
        $deploy->post_deploy = function() use ($deploy) {
          if ($deploy->_success) {
            $result = 'was successful';
          } else {
            $result = 'failed';
          }

          // Send Pushover notification to indicate that code was successfully deployed
          curl_setopt_array($ch = curl_init(), array(
            CURLOPT_URL => 'https://api.pushover.net/1/messages.json',
            CURLOPT_POSTFIELDS => array(
              'token' => 'aCzz3Rz9WWk7ZAWnmBBxTypJnxuP6B',
              'user' => 'u511MMLBp8mHSo6MWDUiYS4HqxU9y8',
              'message' => 'Deployment of ShoePrint $result!',
              'url' => 'http://www.jeffyg.com/shoeprint',
              'url_title' => 'ShoePrint'
            ),
            CURLOPT_SAFE_UPLOAD => true,
          ));
          curl_exec($ch);
          curl_close($ch);
        };

        $deploy->execute();
      }
    }
  } catch (Exception $e) {
    $deploy->log($e, 'ERROR');
  }
} catch (Exception $e) {
  echo 'Exception occurred: ' . $e.getMessage();
}


?>